package io.vsys.hellosse;

import java.io.IOException;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter.SseEventBuilder;

@RestController
public class TriggerController {

    private Logger log = LoggerFactory.getLogger(TriggerController.class);

    @Autowired
    Set<SseEmitter> currentEmitters;

    @PostMapping("/push")
    public void push(@RequestBody Message message) throws IOException {
        log.info("Dispatching message: "+message);
        synchronized(currentEmitters) {
            for (SseEmitter emitter : currentEmitters) {
                if (emitter!=null) {
                    log.info("target emitter="+emitter);
                    SseEventBuilder event = SseEmitter.event()
                        .data(message);
                        //.id(message.getMessage());
                        //.name("hello-sse");
                    emitter.send(event);
                }
            }
        }
       
    }
    
}
