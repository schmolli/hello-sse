# Example for using SSE with Spring Boot

## The application provides a web page that connects the browser to an SSE stream.

[http://localhost:8080/sseclient.html]

## Further, the application provides a REST Service for posting message objects (HTTP POST).
Each message object posted will be forwarded by the service to each connected browser.

`curl -X POST -d '{"message":"Hello!"} http:localhost:8080/push`

Start the service, execute the curl command and check your connected browsers.
You should see the JSON serialization of the message object posted with curl.