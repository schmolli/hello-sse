package io.vsys.hellosse;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Controller
public class HelloSseEmitterController {
    private Logger log = LoggerFactory.getLogger(HelloSseEmitterController.class);   
    

    @Autowired
    private Set<SseEmitter> currentEmitters;

    @GetMapping("/hello-sse")
    public SseEmitter handleSse() {
        SseEmitter emitter = new SseEmitter();
        emitter.onTimeout(()->{
            emitter.complete();
            currentEmitters.remove(emitter);
            log.info("Removed Emitter due timeout: "+emitter);
        });
        currentEmitters.add(emitter);
        log.info("Added new Emitter: "+emitter);
        return emitter;
    }
}
